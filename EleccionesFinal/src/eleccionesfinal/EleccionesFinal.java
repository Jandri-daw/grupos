/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eleccionesfinal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author Alberto
 */
public class EleccionesFinal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in, "ISO-8859-1");
        boolean correcto = false;
        do {
            System.out.println("Dame tu sexo Hombre/Mujer");
            String sexo = sc.nextLine();
            if (sexo.equalsIgnoreCase("Hombre") || sexo.equalsIgnoreCase("Mujer")) {
                correcto = true;
            }
        } while (!correcto);

        int edad = 0;
        String eleccion0 = "";
        int azul = 0;
        int rojo = 0;
        int naranja = 0;
        int verde = 0;
        int morado = 0;
        int totalvotos = 0;
        do {
            System.out.println("Introduce tu edad");
            String edad0 = sc.nextLine();
            try {
                edad = Integer.parseInt(edad0);
                correcto = true;
            } catch (NumberFormatException e) {
                System.out.println("No has introducido un número");
            }
        } while (!correcto);
        if (edad >= 18) {
            System.out.println("Selecciona el partido al que vas a elegir");
            System.out.println("------------------------------------");
            System.out.println("1--Partido Popular");
            System.out.println("2--Partido Socialista Obrero Español");
            System.out.println("3--Ciudadanos");
            System.out.println("4--VOX");
            System.out.println("5--Podemos");
            System.out.println("------------------------------------");
            eleccion0 = sc.nextLine();
            int eleccion = Integer.parseInt(eleccion0);
            switch (eleccion) {
                case 1:
                    azul++;
                    totalvotos++;
                    break;
                case 2:
                    rojo++;
                    totalvotos++;
                    break;
                case 3:
                    naranja++;
                    totalvotos++;
                    break;
                case 4:
                    verde++;
                    totalvotos++;
                    break;
                case 5:
                    morado++;
                    totalvotos++;
                    break;
                default:
                    totalvotos++;
            }
            ArrayList<String> provincias = new ArrayList<String>();

            //provincias de España
            provincias.add("Comunidad Valenciana");
            provincias.add("Comunidad de Madrid");
            provincias.add("Castilla y León");
            provincias.add("Extremadura");
            provincias.add("Andalucía");
            provincias.add("Murcia");
            provincias.add("Catilla la Mancha");
            provincias.add("Aragón");
            provincias.add("Navarra");
            provincias.add("País Vasco");
            provincias.add("Cantabría");
            provincias.add("Asturias");
            provincias.add("La Rioja");
            provincias.add("Galicia");
            provincias.add("Las Baleares");
            provincias.add("Islas Canarias");
            //comprobar elección usuario
            boolean esta = false;
            do {
                System.out.println("Introduce la Comunidad en la que reside de España");
                String seleccion = sc.nextLine();
                Iterator<String> ite = provincias.iterator();
                while (ite.hasNext()) {
                    String comunidades = ite.next();
                    if (seleccion.equals(comunidades)) {
                        esta = true;
                    }
                }
            } while (!esta);

        } else {
            System.out.println("No puedes votar");
        }
    }
}
